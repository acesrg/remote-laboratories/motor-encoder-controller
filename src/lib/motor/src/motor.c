/*
 * Copyright 2022 ACES.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file motor.c */
/* Parameter assignment for PWM. */
#define DEFAULT_PWM_FREQ_HZ                 100     /**< \brief Default PWM frequency. */
#define DRIVER_PWM_COUNT                    1       /**< \brief Quantity of PWMs to use. */
#define DRIVER_PWM_REVERSE                  false   /**< \brief PWM Reverse option. */
#define PWM_DUTY_OFF                        0x0000  /**< \brief Minimum signal value. */
#define PWM_DUTY_MAX                        0xFFFF  /**< \brief Maximum signal value. */
#define PWM_DELAY                           0.0001  /**< \brief Desired delay in seconds. */

/* third party libs */
#include <FreeRTOS.h>
#include <task.h>
#include <pwm.h>

/* third party local libs */
#include <log.h>

/* local libs */
#include <motor.h>

/* configuration includes */
#include <pinout_configuration.h>

/**
 * \brief   PWM configuration structure.
 */
typedef struct pwm_config_t {
    uint16_t freq_hz;   /**< \brief Frequency of the PWM signal in Hertz. */
    uint8_t pin;        /**< \brief Pin where the PWM will be initialized. */
} pwm_config_t;

/**
 * \brief   Global PWM driver configuration.
 */
pwm_config_t pwm_config = {
    .pin = MOTOR_A_ENABLE,
    .freq_hz = DEFAULT_PWM_FREQ_HZ,
};

/**
 * \brief   Function to initialize the motor.
 * \return  RV_OK
 */
retval_t motor_init() {
    log_trace("Set gpio as output for PWM");
    gpio_enable(MOTOR_A_IN1, GPIO_OUTPUT);
    gpio_enable(MOTOR_A_IN2, GPIO_OUTPUT);

    log_trace("Set PWM in pin GPIO%d", pwm_config.pin);
    pwm_init(DRIVER_PWM_COUNT, &pwm_config.pin, DRIVER_PWM_REVERSE);

    log_trace("Set PWM frequency to %d Hz", pwm_config.freq_hz);
    pwm_set_freq(pwm_config.freq_hz);

    log_trace("Set PWM default duty");
    pwm_set_duty(PWM_DUTY_OFF);

    /**
     * \note PWM gpios:
     *       The driver pins are set to 0 so that it does
     *       not rotate in any direction.     
     */
    gpio_write(MOTOR_A_IN1, false);
    gpio_write(MOTOR_A_IN2, false);

    log_trace("Start PWM");
    pwm_start();
    return RV_OK;
}

/**
 * \brief   Function to stop the motor.
 * \return  RV_OK
 */
retval_t motor_deinit() {
    log_trace("Set PWM 0 duty");
    pwm_set_duty(PWM_DUTY_OFF);

    /**
     * \note PWM gpios:
     *       The driver pins are set to 1 so that the motor
     *       performs an immediate braking
     */
    gpio_write(MOTOR_A_IN1, true);
    gpio_write(MOTOR_A_IN2, true);

    log_trace("Stop PWM");
    pwm_stop();
    return RV_OK;
}


/**
 * \brief   Function to choose the sense of rotation of the motor.
 * \param   rotation: ROT_CW or ROT_CCW
 * \return  RV_OK
 */
retval_t motor_rotation(motor_rotation_t rotation) {
    /**
     * \note Rotation workaround: 
     *       If the condition is DIR_CW it is a clockwise rotation and
     *       if it is DIR_CCW it is a counterclockwise rotation.
     */ 
    switch (rotation) {
        case ROT_CW:
            log_trace("Clockwise rotation");
            gpio_write(MOTOR_A_IN1, true);
            gpio_write(MOTOR_A_IN2, false);
            break;

        case ROT_CCW:
            log_trace("Counterclockwise rotation");
            gpio_write(MOTOR_A_IN1, false);
            gpio_write(MOTOR_A_IN2, true);
            break;

        default:
            log_error("Rotation error");
            return RV_EXTERNAL_ERROR;
            break;
    }
    return RV_OK;
}

/**
 * \brief   Function to set a PWM duty value.
 * \param   value: A value in the range from 0x0000 (0%)
 *          to 0xFFFF
 * \return  RV_OK
 */
retval_t motor_set_duty(uint16_t value) {
    /**
     * \note PWM workaround:
     *       To suppress the possibility of unexpected commands
     *       making it to the PWM driver, the duty cycle is
     *       updated ONLY during PWM's low state.
     */
    while (gpio_read(pwm_config.pin)) {
        log_trace("Waiting for duty cycle to end ...");
        vTaskDelay(PWM_DELAY * pwm_config.freq_hz * portTICK_PERIOD_MS);
    }
    log_trace("Set PWM duty: %d", value);
    taskENTER_CRITICAL();
    pwm_set_duty((uint16_t) (value));
    taskEXIT_CRITICAL();
    return RV_OK;
}
