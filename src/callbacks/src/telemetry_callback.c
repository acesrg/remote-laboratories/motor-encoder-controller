/*
 * Copyright 2022 ACES.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file telemetry_callback.c */
/* third party libs */
#include <FreeRTOS.h>
#include <semphr.h>

/* local libs */
#include <json_parser.h>

/* proyect callbacks/cgi */
#include <telemetry_callback.h>

extern simple_json_t motor_db[1];

extern SemaphoreHandle_t xMutex_motor_data;

/**
 * \brief   Called every time telemetry is received, reads and stores telemetry of interest.
 * \param   *pcb: the TCP socket
 * \param   *data: the telemetry string
 * \param   data_len: the telemetry string length in chars
 * \param   mode: the mode in which the telemetry was sent (bits or chars)
 */
retval_t telemetry_callback_handler(struct tcp_pcb *pcb, uint8_t *data, u16_t data_len, uint8_t mode) {
    data[data_len] = '\0';
    /**
     * \note
     *      Since the websocket stream database is being used, and the
     *      json shold have the form of {"duty": %f}. We pass directly
     *      the structure (initialized with said form).
     *
     *      After this call, actuator_db.value should be updated and
     *      ready to use.
     */
    if (xMutex_motor_data != NULL) {
        /* see if we can obtain the motor_db mutex */
        if (xSemaphoreTake(xMutex_motor_data, (TickType_t) 100) == pdTRUE) {
            retval_t parse_rv = quick_get_value((const char *) data, motor_db);

            xSemaphoreGive(xMutex_motor_data);

            if (parse_rv != RV_OK) {
                return RV_ERROR;
            }
        }
    }

    /* if we get to this poin everything went ok! */
    return RV_OK;
}
