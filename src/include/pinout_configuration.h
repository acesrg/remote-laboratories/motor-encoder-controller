/*
 * Copyright 2022 ACES. 
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file pinout_configuration.h */
#ifndef SRC_INCLUDE_PINOUT_CONFIGURATION_H_
#define SRC_INCLUDE_PINOUT_CONFIGURATION_H_

#define MOTOR_A_IN1             5
#define MOTOR_A_IN2             4
#define MOTOR_A_ENABLE          16
#define ENCODER_PIN_A           12
#define ENCODER_PIN_B           13

#endif /* SRC_INCLUDE_PINOUT_CONFIGURATION_H_ */
