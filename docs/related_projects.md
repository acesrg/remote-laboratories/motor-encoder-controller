# Related projects

# The remote control-lab family

The firmware here is part of a bigger project, to gain context or see the application for which it was designed you can peek the following links:

## Nyquist

[Nyquist](https://nyquist.readthedocs.io/en/latest/), is a _Python_ library that helps creating a control algorithm without worrying about the embedded stuff.

## Remote mole

The [Remote Mole](https://remote-mole.readthedocs.io/en/latest/) is a helper that creates Discord bots to communicate with remote servers.

# Some oldies!

## The origins: aeropendulum

This was my control-systems final project at the university. [Aeropendulum](https://github.com/MarcoMiretti/aeropendulum) was a quite different approach to solve the same problem as one of the remote control-lab devices. The idea was the control the angle of a drone-helix-propelled pendulum.

The whole control system ran in a stm32f407 discovery board. While it was fun doing it almost from scratch (without external HALs), the idea was planted then and there... How can someone do this without having to worry about the non control-related knowledge?

